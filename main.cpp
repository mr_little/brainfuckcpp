#include <iostream>
#include <vector>
#include <string>

#include "internal/writter.cpp"
#include "internal/reader.cpp"
#include "internal/basic_interpreter.cpp"
#include "internal/int_stack.cpp"

using namespace std;

int main() {
    Writter *w = new CoutWritter();
    BasicInterpreter *bi = new BasicInterpreter();
    Reader *sr = new StringReader("++++++++++[>+>+++>+++++++>++++++++++<<<<-]>>>++.>+.+++++++..+++.<<++.>+++++++++++++++.>.+++.------.--------.<<+.<.");

    if (bi->Parse(sr) == 0) {
        bi->Interpret(w);
    };

    delete sr;
    delete bi;
    delete w;
}