# Brainfuck C++ interpreter by Boris Rostovskiy #

Brainfuck, language itself, is a Turing-complete language created by Urban Müller. The language only consists of 8 operators, yet with the 8 operators, <>+-[],. you are capable of writing almost any program you can think of.

### Basics ###

* `>` = increases memory pointer, or moves the pointer to the right 1 block.
* `<` = decreases memory pointer, or moves the pointer to the left 1 block.
* `+` = increases value stored at the block pointed to by the memory pointer
* `-` = decreases value stored at the block pointed to by the memory pointer
* `[` = like c while(cur_block_value != 0) loop.
* `]` = if block currently pointed to's value is not zero, jump back to [
* `,` = like c getchar(). input 1 character.
* `.` = like c putchar(). print 1 character to the console

### About this interpreter ###

* Simple, but fully featured
* Two steps implementation used (1st-parsing, 2nd-execute)
* Easy extendable(basically need only to add implementation of new operation and including this operation in parsing cycle)
* Has minimum dependencies


### Basic usage ###

* Run: `docker build --platform linux/x86_64 . -t brainfuck:1.0.0`
* Run: `docker run --platform linux/x86_64 --rm -it brainfuck:1.0.0` and notice that sentence `Hello World!` will be printed out.

### Future improvements ###

* Add more tests
* Add more operations