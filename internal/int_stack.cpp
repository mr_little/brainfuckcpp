# ifndef INTSTACK
# define INTSTACK

#include <iostream>
#include <vector>

class IntStack {
    public:
        IntStack(){}
        ~IntStack() {}
        
        void Push(uint i){
            this->items.push_back(i);
        };
        int Pop() {
            if (!this->items.empty()) {
                uint result = this->items.back();
                this->items.pop_back();
                return (int)result;
            };
            return -1;
        };
    private:
        std::vector<uint> items;
};

# endif