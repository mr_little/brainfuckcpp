#include <iostream>
#include <string>
#include <vector>
#include <map>

#include "int_stack.cpp"
#include "writter.cpp"

class operationArgs {
    public:
        operationArgs(Writter *w, uint *index, uint *pointer, std::vector<uint> &cells, std::map<uint, uint> jmpTable)
        : cells(cells), w(w), index(index), pointer(pointer), jmpTable(jmpTable){};
        
        ~operationArgs(){};

        uint *index;
        uint *pointer;
        Writter *w;

        std::vector<uint> &cells;
        std::map<uint, uint> jmpTable;
};

class operation {
    public:
        virtual ~operation(){};
        virtual void run(uint operationIndex, operationArgs *args) = 0;
        
};

/// Operations implementation
class incr : public operation {
    public:
        ~incr(){};
        void run(uint operationIndex, operationArgs *args) {
            args->cells[*args->pointer]++;
        };
};

class decr : public operation {
    public:
        ~decr(){};
        void run(uint operationIndex, operationArgs *args) {
            args->cells[*args->pointer]--;
        };
};

class printout : public operation {
    public:
        ~printout(){};
        void run(uint operationIndex, operationArgs *args) {
            char c = args->cells[*args->pointer];
            args->w->Write(std::string(1, c));
        };
};

class shiftRight : public operation {
    public:
        ~shiftRight(){};
        void run(uint operationIndex, operationArgs *args) {
            (*args->pointer)++;
        };
};

class shiftLeft : public operation {
    public:
        ~shiftLeft(){};
        void run(uint operationIndex, operationArgs *args) {
            (*args->pointer)--;
        };
};

class jmp : public operation {
    public:
        ~jmp(){};

        void run(uint operationIndex, operationArgs *args) {
            // if no more cycles left
            if (args->cells[*args->pointer] <= 0) {

                // check that key is exists
                if (args->jmpTable.count(operationIndex) > 0) {
                    uint to = args->jmpTable[operationIndex];
                    // jump to the end of the cycle
                    *args->index = to;
                }
	        }
        };
};

class ret : public operation {
    public:
        ~ret(){};

        void run(uint operationIndex, operationArgs *args) {
            // if more cycles iterations left
            if (args->cells[*args->pointer] > 0) {
                // check that key is exists

                if (args->jmpTable.count(operationIndex) > 0) {
                    uint to = args->jmpTable[operationIndex];

                    // jump to the begin of the cycle
                    (*args->index) = to;
                }
	        }
        };
};