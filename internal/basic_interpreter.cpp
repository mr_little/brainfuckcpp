#include <vector>
#include <string>

#include "reader.cpp"
#include "operations.cpp"

class Interpreter {
    public:
        ~Interpreter(){};
        virtual int Parse(Reader *r) = 0;
        virtual void Interpret(Writter *w) = 0;

    protected:
        virtual void initOperationsMap() = 0;
        std::vector<operation*> items;
        std::map<char, operation*> operationsMap;
        std::map<uint, uint> jmpTable;
};

class BasicInterpreter : public Interpreter {
    public:

        BasicInterpreter() {
            this->initOperationsMap();
        }

        ~BasicInterpreter() {
            std::map<char,operation *>::iterator iter = this->operationsMap.begin();
            std::map<char,operation *>::iterator endIter = this->operationsMap.end();
            for(; iter != endIter; ++iter) {
                delete this->operationsMap[iter->first];
            }
        }

        int Parse(Reader *r) {
            char *buff[1];
            IntStack *is = new IntStack();
            uint i = 0;

            r->Read(buff);
            while (buff[0]) {
                if (this->operationsMap.count(*buff[0]) > 0) {
                    operation *op = this->operationsMap[*buff[0]];

                    if (*buff[0] == '['){
                        is->Push(i);
                    } else if (*buff[0] == ']') {
                        uint from = is->Pop();
                        this->jmpTable[from] = i;
                        this->jmpTable[i] = from;
                    }

                    this->items.push_back(op);

                } else {
                    i++;
                    continue;
                }

                r->Read(buff);
                i++;
            }
            
            delete buff[0];
            delete is;

            return 0;
        }

        void Interpret(Writter *w){
            std::vector <uint> cells;
            cells.reserve(1000);
            uint pointer = 0;

            for (uint i = 0; i < this->items.size(); i++) {
                operationArgs *oa = new operationArgs(w, &i, &pointer, cells, this->jmpTable);
                operation *op = this->items[i];
                op->run(i, oa);
            }
        }

    protected:
        void initOperationsMap() {
            this->operationsMap['+'] = new incr();
            this->operationsMap['-'] = new decr();
            this->operationsMap['>'] = new shiftRight();
            this->operationsMap['<'] = new shiftLeft();
            this->operationsMap['['] = new jmp();
            this->operationsMap[']'] = new ret();
            this->operationsMap['.'] = new printout();
        }
};