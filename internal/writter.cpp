# ifndef WRITTER
# define WRITTER

#include <iostream>
#include <string>

class Writter {
    public:
        virtual ~Writter(){};
        virtual void Write(std::string str) = 0;
        virtual void Writeln(std::string str) = 0;
};

class CoutWritter : public Writter{
    public:
        ~CoutWritter(){};
        void Write(std::string str){
            std::cout << str;        
        };
        
        void Writeln(std::string str){
            std::cout << str << std::endl;        
        };
};

# endif