#include <iostream>
# ifndef READER
# define READER

#include <string>
#include <vector>

class Reader {
    public: 
        virtual ~Reader(){}
        virtual void Read(char *returnValue[1]) = 0;
};

class StringReader : public Reader{
    public:
        StringReader(std::string str) {
            std::reverse(str.begin(), str.end());
            for(std::string::iterator it = str.begin(); it != str.end(); it++) {
                char c = *it;
                this->src.push_back(c);
            }
        };

        ~StringReader(){};

        void Read(char* returnValue[1]) {
            returnValue[0] = nullptr;

            if (!this->src.empty()) {
                returnValue[0] = (&this->src.back());
                this->src.pop_back();
            }
        };
    private:
        std::vector<char> src;
};

# endif